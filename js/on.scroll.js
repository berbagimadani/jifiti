/********************************************************/
/*      ON.SCROLL - CONFIG                              */
/********************************************************/

var navClass = 'navigation';
var activeNavItemClass = 'active';
var sectionAnchorClass = 'section';
var visibleFromBottom = 100; //how m
var scrollLock = false;
var sections = [];
var sectionsCount = 0;         

function setActiveNavItem(itemId){
	  //$('.'+ navClass +' a').toggleClass(activeNavItemClass, false);
	  //$('.' + navClass).find('a[href="#' + itemId + '"]').toggleClass(activeNavItemClass, true); 
};

 
var Section = function(_id, _top){
	this.id = _id;
	this.top = Math.round(_top); //px
	this.select = function(){
		setActiveNavItem(this.id);
	};
};

$(window).bind('scroll',function(e){ 
		 var position = $(window).scrollTop();
    	 //$('.tjs').text(position+"s");
    	 //if(position == 0){ 
    	 	//$('.r1').fadeIn();
			//$('.r2').animate({top:"48px"}, {duration: 'fast'});
    	 //}
    	 
    	var itemId = position; 

    	/* nav active */
 		if(itemId < 150){
 			$('.'+ navClass +' li').toggleClass(activeNavItemClass, false);
	  		$('.' + navClass).find('li[id="#row1"]').toggleClass(activeNavItemClass, true);
 		}
 		if(itemId > 1326){
 			$('.'+ navClass +' li').toggleClass(activeNavItemClass, false);
	  		$('.' + navClass).find('li[id="#row6"]').toggleClass(activeNavItemClass, true);
 		}
 		if(itemId > 2404){
 			$('.'+ navClass +' li').toggleClass(activeNavItemClass, false);
	  		$('.' + navClass).find('li[id="#row7"]').toggleClass(activeNavItemClass, true);
 		}
 		
 		if(itemId > 2974){
 			$('.'+ navClass +' li').toggleClass(activeNavItemClass, false);
	  		$('.' + navClass).find('li[id="#row9"]').toggleClass(activeNavItemClass, true);
 		}
 		if(itemId > 3480){
 			$('.'+ navClass +' li').toggleClass(activeNavItemClass, false);
	  		$('.' + navClass).find('li[id="#row10"]').toggleClass(activeNavItemClass, true);
 		}
 		if(itemId > 4270){
 			$('.'+ navClass +' li').toggleClass(activeNavItemClass, false);
	  		$('.' + navClass).find('li[id="#row12"]').toggleClass(activeNavItemClass, true);
 		}
 		/* nav active */

 		if(itemId > 600){
 			$('.row3-guy1').fadeOut();
 			$('.row3-guy2').fadeOut();
 		}
 		if(itemId < 600){
 			$('.row3-guy1').fadeIn();
 			$('.row3-guy2').fadeIn();
 		}


	/* TESTIMONI */
 		if(itemId < 700){ 
			 $('#row5').fadeOut().hide();
		}
		if(itemId > 900 && itemId < 1200){ 
			$('#row5').fadeIn('slow').show();
		}
		if(itemId > 1290 ){ 
			//$('#row5').fadeOut().hide();
		}
		/* */
 		if(itemId < 2290){ 
			 $('#row8').fadeOut().hide();
		}
		if(itemId >= 2500 && itemId < 2850){ 
			$('#row8').fadeIn('slow').show();
		}
		if(itemId > 3030){ 
			//$('#row8').fadeOut('slow').hide();
		}
		/* */
		if(itemId < 3465){ 
			 $('.row11').fadeOut().hide();
		}
		if(itemId >= 3765 && itemId < 4060){ 
			$('.row11').fadeIn('slow').show();
		}
		if(itemId > 4100){ 
			 //$('.row11').fadeOut().hide();
		}

		/* */
		if(itemId > 4120){ 
			 $('#row5').show();
			 $('#row8').show();
			 $('.row11').show();
		}
		/* END TESTIMONI */ 
		
		if(itemId > 3120){
			//$('.row10-iphone').show().animate({right:"75px"}, {duration: 3000});
		}
		if(itemId > 3120){
			//$('.row10-iphone').show().animate({left:"75px"}, {duration: 3000});
		}

		/* Scenarios */
		if(itemId < 1000){
			$('#row6-arrow-left-parallax').animate({'height': '0'}, 100);
			$('#row6-arrow-right-parallax').animate({'height': '0'}, 100);
		}
		if(itemId > 1650 && itemId < 2200){
			$('#row6-arrow-left-parallax').animate({'height': '150px'}, 600);
			$('#row6-arrow-right-parallax').animate({'height': '148px'}, 600);
		}
		if(itemId > 2210){
			$('#row6-arrow-left-parallax').animate({'height': '0'}, 100);
			$('#row6-arrow-right-parallax').animate({'height': '0'}, 100);
		}

		/* */
		$('.bg-rotate').stop(true,true); 
		if(itemId < 1900){ 
			$('.bg-rotate').animate({rotate: '+=36'}, 4000);
		}
		if(itemId > 1800){ 
			$('.bg-rotate').animate({rotate: '+=36'}, 4000);
		}
		if(itemId < 2300){
			//$('.bg-rotate').stop(true,false);
			//$('.bg-rotate').animate({rotate: '+=36'}, 4000);
		}


});	
    
var markNav = (function() {
	if(!scrollLock){
		var position = $(window).scrollTop();
				
		if(position == 0){
			setActiveNavItem(sections[0].id);
			return true;
		}
		var i = 0;		
		while(i < sectionsCount-1 && ((sections[i+1].top - position) < ($(window).height() - visibleFromBottom)) )
		{
			i++;

		}

		setActiveNavItem(sections[i].id);


	}
});

$(document).ready(function() {
	//var tx = '';
	$('.'+ navClass +' li').toggleClass(activeNavItemClass, false);
	$('.' + navClass).find('li[id="#row1"]').toggleClass(activeNavItemClass, true);
	
	/* TESTIMONI */
	//$('#testi1').hide();
	$('#row5').hide();
	$('#row8').hide();
	$('.row11').hide();
	/* END TESTIMONI */
 
	
	//$('.row10-iphone').hide();

	var anchors = $('.' + sectionAnchorClass);
	sectionsCount = anchors.length;
	var position = 0;
	for(i=0; i<sectionsCount; i++){
		position = $(anchors[i]).offset();
		sections[i] = new Section($(anchors[i]).attr('id'),  position.top);

	}
	markNav();
	$(window).scroll(function() {
		markNav();
		$('body').append($('<div></div>').addClass('iosfix'));
		setTimeout(function() {
			$('.iosfix').remove();
			//$('#row1').hide();  
		}, 500);
	});
	$('.navigation a').each(function(){
		$(this).click(function(event){
			/* acnhor disable/enabled
			event.preventDefault();
			*/
			scrollLock = true;
			var itemId = ($(this).attr('href')).replace('#', '');
			var position = $('div#' + itemId).position();
			$('html,body').animate(
				{ scrollTop: position.top+'px' },
				1300, 
				function() {
					scrollLock = false;
					//setActiveNavItem(itemId);
					 //$('.tjs').text("="+itemId); 
					 $('.'+ navClass +' li').toggleClass(activeNavItemClass, false);
	  				 $('.' + navClass).find('li[id="#' + itemId + '"]').toggleClass(activeNavItemClass, true); 
 
				}	
			);
			
		});
	});
});